from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('experiences', views.experiences, name='experiences')
]