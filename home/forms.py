from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'matakuliah',
            'dosen',
            'sks',
            'deskripsi',
            'semester',
            'kelas'
        ]
        widgets = {
            'matakuliah':forms.TextInput(attrs={'class':'form-control'}),
            'dosen':forms.TextInput(attrs={'class':'form-control'}),
            'sks':forms.TextInput(attrs={'class':'form-control'}),
            'deskripsi':forms.Textarea(attrs={'class':'form-control'}),
            'semester':forms.TextInput(attrs={'class':'form-control'}),
            'kelas':forms.TextInput(attrs={'class':'form-control'})
        }