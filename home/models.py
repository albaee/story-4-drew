from django.db import models

class Matkul(models.Model):
    matakuliah = models.CharField(max_length=300)
    dosen = models.CharField(max_length=300)
    sks = models.CharField(max_length=300)
    deskripsi = models.TextField()
    semester = models.CharField(max_length=300)
    kelas = models.CharField(max_length=300)

    def __str__(self):
        return self.matakuliah
# Create your models here.
