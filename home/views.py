from django.shortcuts import render
from .forms import MatkulForm
from .models import Matkul
from django.views.generic import ListView, DetailView, DeleteView
from django.urls import reverse_lazy
# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def about(request):
    return render(request, 'home/about.html')

def projects(request):
    return render(request, 'home/projects.html')

def matkul_add(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()

    context = {
        "form":form
    }
    return render(request, 'home/matakuliah-add.html', context)

class MatkulView(ListView):
    model = Matkul
    template_name = "home/matakuliah.html"

class MatkulDetail(DetailView):
    model = Matkul
    template_name = "home/matakuliah-details.html"

class MatkulDelete(DeleteView):
    model = Matkul
    template_name = "home/matakuliah-delete.html"
    success_url = reverse_lazy('home:matakuliah')