from django.test import TestCase, Client
from .models import Matkul
# Create your tests here.

class StoryTester(TestCase):
    def test_model_matkul(self):
        mat = Matkul.objects.create(
            matakuliah = "test",
            dosen = "test",
            sks = "test",
            deskripsi = "test",
            semester = "test",
            kelas = "test"
            )
        counter = Matkul.objects.all().count()
        self.assertEqual(counter,1)
        self.assertEqual(mat.__str__(),mat.matakuliah)

    def test_url_matkul(self):
        response = Client().get('/matakuliah')
        self.assertEquals(response.status_code,200)

    def test_url_index(self):
        response = Client().get('/')
        self.assertEquals(response.status_code,200)
    
    def test_url_about(self):
        response = Client().get('/about')
        self.assertEquals(response.status_code,200)

    def test_url_projects(self):
        response = Client().get('/projects')
        self.assertEquals(response.status_code,200)
    
    def test_url_projects(self):
        response = Client().get('/matakuliah-add')
        self.assertEquals(response.status_code,200)
    
    def test_url_projects(self):
        response = Client().get('/projects')
        self.assertEquals(response.status_code,200)

    def test_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/index.html')
    
    def test_about_template(self):
        response = Client().get('/about')
        self.assertTemplateUsed(response, 'home/about.html')

    def test_projects_template(self):
        response = Client().get('/projects')
        self.assertTemplateUsed(response, 'home/projects.html')
    
    def test_matakuliah_template(self):
        response = Client().get('/matakuliah')
        self.assertTemplateUsed(response, 'home/matakuliah.html')