from django.test import TestCase, Client
from .views import index, search
from django.urls import resolve


class TestSearch(TestCase):
    def test_url(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

    def test_view(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_url_data(self):
        response = Client().get('/story8/data?q=test')
        self.assertEquals(response.status_code, 301)

    def test_template(self):
        response = Client().get('/story8/')
        html_test = response.content.decode('utf8')
        self.assertIn("show", html_test)
        self.assertIn("javascript", html_test)
        self.assertIn("searchbar", html_test)
        
