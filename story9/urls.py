from django.urls import path, include
from .views import UserRegisterView

app_name = 'story9'

urlpatterns = [
    path('register/',UserRegisterView.as_view(), name="register")
]
