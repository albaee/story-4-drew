from django.urls import path
from . import views
from .views import KegiatanView, AddKegiatanView, AddMemberView

app_name = 'story6'

urlpatterns = [
    path('kegiatan', KegiatanView.as_view(), name='kegiatan'),
    path('add-kegiatan', AddKegiatanView.as_view(), name='add-kegiatan'),
    path('add-member', AddMemberView.as_view(), name='add-member')
]