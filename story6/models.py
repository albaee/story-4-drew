from django.db import models
from django.urls import reverse

# Create your models here.
class Kegiatan(models.Model):
    activity = models.CharField(max_length=300)

    def __str__(self):
        return self.activity

    def get_absolute_url(self):
        return reverse("story6:kegiatan")
    

class Attendee(models.Model):
    attendee = models.CharField(max_length=300)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.attendee

    def get_absolute_url(self):
        return reverse("story6:kegiatan")