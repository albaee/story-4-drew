from django.contrib import admin
from .models import Kegiatan, Attendee
# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Attendee)
